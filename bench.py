import platform
import sys
import time
import math
import random

MILLIS_IN_SECOND = 1000


def print_and_flush(*args, sep=' ', end='\n', file=None):
    print(*args, sep=sep, end=end, file=file)
    sys.stdout.flush()


def bench_sort(list_size):
    start_alloc = time.perf_counter()
    numbers = allocate_list(list_size)
    alloc_duration = time.perf_counter() - start_alloc

    start_popul = time.perf_counter()
    populate_with_random(numbers)
    popul_duration = time.perf_counter() - start_popul

    start_sort = time.perf_counter()
    numbers.sort()
    sort_duration = time.perf_counter() - start_sort

    return [alloc_duration, popul_duration, sort_duration]


def allocate_list(list_size):
    return [0] * list_size


def populate_with_random(numbers):
    for i in range(len(numbers)):
        numbers[i] = math.floor(random.random() * 10)


def get_python_version():
    return "Python " + platform.python_version()


def main():
    list_size = int(sys.argv[1])
    number_of_benches = int(sys.argv[2])

    print_and_flush("Running %s bench with list size of %d, %d times" % (get_python_version(), list_size, number_of_benches))

    alloc_sum = 0.0
    popul_sum = 0.0
    sort_sum = 0.0
    for bench_nr in range(1, number_of_benches + 1):
        durations = bench_sort(list_size)
        alloc_sum += durations[0]
        popul_sum += durations[1]
        sort_sum += durations[2]

        print_and_flush("%d/%d benches completed" % (bench_nr, number_of_benches), end="\r")

    print_and_flush("%s average allocation duration %fms" %
          (get_python_version(), (alloc_sum * MILLIS_IN_SECOND) / number_of_benches))
    print_and_flush("%s average random population duration %fms" %
          (get_python_version(), (popul_sum * MILLIS_IN_SECOND) / number_of_benches))
    print_and_flush("%s average sort duration %fms" %
          (get_python_version(), (sort_sum * MILLIS_IN_SECOND) / number_of_benches))


if __name__ == "__main__":
    main()
