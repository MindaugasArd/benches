#include <chrono>
#include <algorithm>
#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <array>

std::string get_cpp_version() {
#ifdef __cplusplus
    switch (__cplusplus) {
        case 1L:
            return "pre C++98";
        case 199711L:
            return "C++98";
        case 201103L:
            return "C++11";
        case 201402L:
            return "C++14";
        case 201703L:
            return "C++17";
        default:
            return "unknown";
    }
#else
    return "unknown";
#endif
}

std::string get_cpp_bench_name() {
    return get_cpp_version() + " int[] ";
}

int *allocate_array(int array_size) {
    return new int[array_size];
}

void populate_with_random(int *numbers, int array_size) {
    for (auto i = 0; i < array_size; ++i) {
        numbers[i] = std::rand() % 10;
    }
}

std::array<long double, 3> bench_sort(int array_size) {

    auto start_alloc = std::chrono::high_resolution_clock::now();
    auto *numbers = allocate_array(array_size);
    auto end_alloc = std::chrono::high_resolution_clock::now();

    auto start_popul = std::chrono::high_resolution_clock::now();
    populate_with_random(numbers, array_size);
    auto end_popul = std::chrono::high_resolution_clock::now();

    auto start_sort = std::chrono::high_resolution_clock::now();
    std::sort(numbers, numbers + array_size);
    auto end_sort = std::chrono::high_resolution_clock::now();

    delete[] numbers;

    auto alloc_duration = std::chrono::duration<long double, std::milli>(end_alloc - start_alloc).count();
    auto popul_duration = std::chrono::duration<long double, std::milli>(end_popul - start_popul).count();
    auto sort_duration = std::chrono::duration<long double, std::milli>(end_sort - start_sort).count();

    return {alloc_duration, popul_duration, sort_duration};
}

int main(int argc, char **argv) {

    auto array_size = std::stoi(argv[1]);
    auto number_of_benches = std::stoi(argv[2]);

    std::cout << "Running " << get_cpp_bench_name() << " bench with array size of "
            << array_size << ", " << number_of_benches << " times" << std::endl;

    long double alloc_sum = 0.0;
    long double popul_sum = 0.0;
    long double sort_sum = 0.0;
    for (auto bench_nr = 1; bench_nr <= number_of_benches; ++bench_nr) {
        auto durations = bench_sort(array_size);
        alloc_sum += durations[0];
        popul_sum += durations[1];
        sort_sum += durations[2];

        std::cout << bench_nr << "/" << number_of_benches << " benches completed\r" << std::flush;
    }

    std::cout << get_cpp_bench_name() << " average allocation duration "
            << (alloc_sum / number_of_benches) << "ms" << std::endl;
    std::cout << get_cpp_bench_name() << " average random population duration "
            << (popul_sum / number_of_benches) << "ms" << std::endl;
    std::cout << get_cpp_bench_name() << " average sort duration "
            << (sort_sum / number_of_benches) << "ms" << std::endl;

    return 0;
}

