import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public final class Bench {

    private static final ThreadLocalRandom RANDOM = ThreadLocalRandom.current();
    private static final double NANOS_IN_MILLI = 1_000_000.0;

    private Bench() {
        throw new AssertionError("Can not instantiate Bench");
    }

    public static void main(String[] args) {

        int arraySize = Integer.parseInt(args[0]);
        int numberOfBenches = Integer.parseInt(args[1]);

        System.out.println(String.format("Running %s bench with array size of %d, %d times",
                getJavaVersion(), arraySize, numberOfBenches));

        long allocSum = 0;
        long populSum = 0;
        long sortSum = 0;
        for (int benchNr = 1; benchNr <= numberOfBenches; ++benchNr) {
            long[] durations = benchSort(arraySize);
            allocSum += durations[0];
            populSum += durations[1];
            sortSum += durations[2];

            System.out.print(String.format("%d/%d benches completed\r", benchNr, numberOfBenches));
        }

        System.out.println(String.format("%s average allocation duration %fms",
                getJavaVersion(), (allocSum / NANOS_IN_MILLI) / numberOfBenches));
        System.out.println(String.format("%s average random population duration %fms",
                getJavaVersion(), (populSum / NANOS_IN_MILLI) / numberOfBenches));
        System.out.println(String.format("%s average sort duration %fms",
                getJavaVersion(), (sortSum / NANOS_IN_MILLI) / numberOfBenches));
    }

    private static long[] benchSort(int arraySize) {

        long startAlloc = System.nanoTime();
        int[] numbers = allocateArray(arraySize);
        long allocDuration = System.nanoTime() - startAlloc;

        long startPopul = System.nanoTime();
        populateWithRandom(numbers);
        long populDuration = System.nanoTime() - startPopul;

        long startSort = System.nanoTime();
        Arrays.sort(numbers);
        long sortDuration = System.nanoTime() - startSort;

        return new long[]{allocDuration, populDuration, sortDuration};
    }

    private static int[] allocateArray(int arraySize) {
        return new int[arraySize];
    }

    private static void populateWithRandom(int[] numbers) {
        for (int i = 0; i < numbers.length; ++i) {
            numbers[i] = RANDOM.nextInt(10);
        }
    }

    private static String getJavaVersion() {
        return "Java int[] " + System.getProperty("java.version");
    }
}
