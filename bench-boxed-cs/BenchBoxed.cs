using System;
using System.Diagnostics;
using System.Collections.Generic;

class BenchBoxed
{
    private static Random RANDOM = new Random();

    public static int Main(string[] args)
    {
        var listSize = Int32.Parse(args[0]);
        var numberOfBenches = Int32.Parse(args[1]);

        Console.WriteLine(String.Format("Running {0} bench with list size of {1}, {2} times",
                        GetCSharpVersion(), listSize, numberOfBenches));

                var allocSum = 0.0d;
                var populSum = 0.0d;
                var sortSum = 0.0d;
                for (var benchNr = 1; benchNr <= numberOfBenches; ++benchNr)
                {
                    var durations = BenchSort(listSize);
                    allocSum += durations[0];
                    populSum += durations[1];
                    sortSum += durations[2];

                    Console.Write(String.Format("{0}/{1} benches completed\r", benchNr, numberOfBenches));
                }

                Console.WriteLine(String.Format("{0} average allocation duration {1}ms",
                        GetCSharpVersion(), allocSum / numberOfBenches));
                Console.WriteLine(String.Format("{0} average random population duration {1}ms",
                        GetCSharpVersion(), populSum / numberOfBenches));
                Console.WriteLine(String.Format("{0} average sort duration {1}ms",
                        GetCSharpVersion(), sortSum / numberOfBenches));
        return 0;
    }

    private static double[] BenchSort(int listSize)
    {
        var allocateWatch = Stopwatch.StartNew();
        var numbers = AllocateList(listSize);
        allocateWatch.Stop();
        var allocDuration = allocateWatch.Elapsed.TotalMilliseconds;

        var populWatch = Stopwatch.StartNew();
        PopulateWithRandom(numbers, listSize);
        populWatch.Stop();
        var populDuration = populWatch.Elapsed.TotalMilliseconds;

        var sortWatch = Stopwatch.StartNew();
        numbers.Sort();
        sortWatch.Stop();
        var sortDuration = sortWatch.Elapsed.TotalMilliseconds;

        return new double[]{allocDuration, populDuration, sortDuration};
    }

    private static List<int> AllocateList(int listSize)
    {
        return new List<int>(listSize);
    }

    private static void PopulateWithRandom(List<int> numbers, int listSize)
    {
        for (var i = 0; i < listSize; ++i)
        {
            numbers.Add(RANDOM.Next(0, 10));
        }
    }

    private static string GetCSharpVersion()
    {
        return "C# List<int> " + Environment.Version.ToString();
    }
}
