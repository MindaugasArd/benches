<?php

const MILLIS_IN_SECOND = 1000.0;
function bench_sort(int $array_size): array {
    $start_alloc = microtime(true);
    $numbers = allocate_array($array_size);
    $alloc_duration = microtime(true) - $start_alloc;

    $start_popul = microtime(true);
    populate_with_random($numbers);
    $popul_duration = microtime(true) - $start_popul;

    $start_sort = microtime(true);
    sort($numbers);
    $sort_duration = microtime(true) - $start_sort;

    return [$alloc_duration, $popul_duration, $sort_duration];
}

function allocate_array(int $array_size): array {
    return array_fill(0, $array_size, 0);
}

function populate_with_random(array &$numbers): void {
    for ($i = 0; $i < count($numbers); ++$i) {
        $numbers[$i] = rand(0, 10);
    }
}

function get_php_version(): string {
    return "PHP " . phpversion();
}

function main(array &$argv): void {
    $array_size = intval($argv[1]);
    $number_of_benches = intval($argv[2]);

    printf("Running %s bench with array size of %d, %d times\n",
        get_php_version(), $array_size, $number_of_benches);

    $alloc_sum = 0.0;
    $popul_sum = 0.0;
    $sort_sum = 0.0;
    for ($bench_nr = 1; $bench_nr <= $number_of_benches; ++$bench_nr) {
        $durations = bench_sort($array_size);
        $alloc_sum += $durations[0];
        $popul_sum += $durations[1];
        $sort_sum += $durations[2];

        printf("%d/%d benches completed\r", $bench_nr, $number_of_benches);
    }

    printf("%s average allocation duration %fms\n",
        get_php_version(), ($alloc_sum * MILLIS_IN_SECOND) / $number_of_benches);
    printf("%s average random population duration %fms\n",
        get_php_version(), ($popul_sum * MILLIS_IN_SECOND) / $number_of_benches);
    printf("%s average sort duration %fms\n",
        get_php_version(), ($sort_sum * MILLIS_IN_SECOND) / $number_of_benches);
}

main($argv);

?>