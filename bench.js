const {
    performance
} = require('perf_hooks');

function populateWithRandom(numbers) {
    for (let i = 0; i < numbers.length; ++i) {
        numbers[i] = Math.floor(Math.random() * 10);
    }
}

function allocateArray(arraySize) {
    return new Array(arraySize);
}

function benchSort(arraySize) {
    const startAlloc = performance.now();
    const numbers = allocateArray(arraySize);
    const allocDuration = performance.now() - startAlloc;

    const startPopul = performance.now();
    populateWithRandom(numbers);
    const populDuration = performance.now() - startPopul;

    const startSort = performance.now();
    numbers.sort();
    const sortDuration = performance.now() - startSort;

    return [allocDuration, populDuration, sortDuration];
}

function getNodeVersion() {
    return "Node.js " + process.version;
}

function main() {
    const arraySize = parseInt(process.argv[2]);
    const numberOfBenches = parseInt(process.argv[3]);

    console.log("Running %s bench with array size of %d, %d times", 
        getNodeVersion(), arraySize, numberOfBenches);

    let allocSum = 0.0;
    let populSum = 0.0;
    let sortsum = 0.0;
    for (let benchNr = 1; benchNr <= numberOfBenches; ++benchNr) {
        let durations = benchSort(arraySize);
        allocSum += durations[0];
        populSum += durations[1];
        sortsum += durations[2];

		process.stdout.write( benchNr + "/" + numberOfBenches + " benches completed\r");
    }

    console.log("%s average allocation duration %fms", getNodeVersion(), allocSum / numberOfBenches);
    console.log("%s average random population duration %fms", getNodeVersion(), populSum / numberOfBenches);
    console.log("%s average sort duration %fms", getNodeVersion(), sortsum / numberOfBenches);
}

main();