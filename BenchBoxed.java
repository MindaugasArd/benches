import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public final class BenchBoxed {

    private static final ThreadLocalRandom RANDOM = ThreadLocalRandom.current();
    private static final double NANOS_IN_MILLI = 1_000_000.0;

    private BenchBoxed() {
        throw new AssertionError("Can not instantiate Bench");
    }

    public static void main(String[] args) {

        int listSize = Integer.parseInt(args[0]);
        int numberOfBenches = Integer.parseInt(args[1]);

        System.out.println(String.format("Running %s bench with list size of %d, %d times",
                getJavaVersion(), listSize, numberOfBenches));

        long allocSum = 0;
        long populSum = 0;
        long sortSum = 0;
        for (int benchNr = 1; benchNr <= numberOfBenches; ++benchNr) {
            long[] durations = benchSort(listSize);
            allocSum += durations[0];
            populSum += durations[1];
            sortSum += durations[2];

            System.out.print(String.format("%d/%d benches completed\r", benchNr, numberOfBenches));
        }

        System.out.println(String.format("%s average allocation duration %fms",
                getJavaVersion(), (allocSum / NANOS_IN_MILLI) / numberOfBenches));
        System.out.println(String.format("%s average random population duration %fms",
                getJavaVersion(), (populSum / NANOS_IN_MILLI) / numberOfBenches));
        System.out.println(String.format("%s average sort duration %fms",
                getJavaVersion(), (sortSum / NANOS_IN_MILLI) / numberOfBenches));
    }

    private static long[] benchSort(int listSize) {

        long startAlloc = System.nanoTime();
        List<Integer> numbers = allocateList(listSize);
        long allocDuration = System.nanoTime() - startAlloc;

        long startPopul = System.nanoTime();
        populateWithRandom(numbers, listSize);
        long populDuration = System.nanoTime() - startPopul;

        long startSort = System.nanoTime();
        Collections.sort(numbers);
        long sortDuration = System.nanoTime() - startSort;

        return new long[]{allocDuration, populDuration, sortDuration};
    }

    private static List<Integer> allocateList(int listSize) {
        return new ArrayList<>(listSize);
    }

    private static void populateWithRandom(List<Integer> numbers, int listSize) {
        for (int i = 0; i < listSize; ++i) {
            numbers.add(RANDOM.nextInt(10));
        }
    }

    private static String getJavaVersion() {
        return "Java List<Integer> " + System.getProperty("java.version");
    }
}
