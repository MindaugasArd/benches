from __future__ import print_function

import os
import subprocess
import sys
import time

import matplotlib.pyplot as plt
import numpy as np
from decimal import Decimal

DEFAULT_NUMBER_OF_BENCHES = 1_000
DEFAULT_ARRAY_SIZE = 1_000_000
TWOPLACES = Decimal(10) ** -2

class BenchMeta:
    array_size = int(sys.argv[1]) if len(sys.argv) >= 2 else DEFAULT_ARRAY_SIZE
    number_of_benches = int(sys.argv[2]) if len(sys.argv) >= 3 else DEFAULT_NUMBER_OF_BENCHES

    def __init__(self, name, compile_command, run_command):
        self.name = name
        self.compile_command = compile_command
        self.run_command = run_command + [str(BenchMeta.array_size), str(BenchMeta.number_of_benches)]


def print_error(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def extract_time(line):
    time_word = line.split(" ")[-1]
    time_str = time_word[:time_word.rfind("ms")].replace(",", ".")
    return round(float(time_str), 4)


def plot(array_size, number_of_benches, results):
    N = len(results)

    ind = np.arange(N)
    width = 0.30
    fig, ax = plt.subplots()

    dpi = 96
    fig.set_size_inches((1280 / dpi, 960 / dpi))
    fig.set_dpi(dpi)

    titles = []
    alloc = []
    random = []
    sort = []
    for key, result in results.items():
        titles.append(key)
        alloc.append(result["allocTime"])
        random.append(result["randomPopulTime"])
        sort.append(result["sortTime"])

    rects1 = ax.bar(ind, alloc, width)
    rects2 = ax.bar(ind + width, random, width)
    rects3 = ax.bar(ind + (width * 2), sort, width)

    ax.set_ylabel('Time (ms)')
    ax.set_title('Benchmark results average of %d sorting %d elements' % (number_of_benches, array_size))
    ax.set_xticks(ind + width)
    ax.set_xticklabels(titles)

    ax.legend((rects1[0], rects2[0], rects3[0]), ('Allocation', 'Random population', "Sort"))

    height_offset = max(alloc + random + sort) * 0.005

    def autolabel(rects):
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x() + rect.get_width() / 2.0, height + height_offset,
                    '%s' % Decimal(str(round(height, 2))).quantize(TWOPLACES),
                    ha='center', va='bottom')

    autolabel(rects1)
    autolabel(rects2)
    autolabel(rects3)

    plt.savefig("bench.png", dpi = dpi)
    plt.show()


def main():
    curr_path = os.path.dirname(os.path.realpath(__file__))

    benches = [
        BenchMeta("C# int[]", ["dotnet", "build", "%s/bench-cs/benches.csproj" % curr_path], ["dotnet", "run", "--project", "%s/bench-cs/benches.csproj" % curr_path, "--"]),
        BenchMeta("C# List<int>", ["dotnet", "build", "%s/bench-boxed-cs/benches-boxed.csproj" % curr_path], ["dotnet", "run", "--project", "%s/bench-boxed-cs/benches-boxed.csproj" % curr_path, "--"]),
        BenchMeta("Java int[]", ["javac", "%s/Bench.java" % curr_path], ["java", "-cp", curr_path, "Bench"]),
        BenchMeta("Java List<Integer>", ["javac", "%s/BenchBoxed.java" % curr_path], ["java", "-cp", curr_path, "BenchBoxed"]),
        BenchMeta("C++ int[]",
                  ["g++", "-std=c++17", "-O3",
                   # "-fopenmp", "-D_GLIBCXX_PARALLEL",
                   "%s/bench.cpp" % curr_path, "-o", "%s/bench" % curr_path],
                  ["%s/bench" % curr_path]),
        BenchMeta("C++ std::vector<int*>",
                  ["g++", "-std=c++17", "-O3",
                   # "-fopenmp", "-D_GLIBCXX_PARALLEL",
                   "%s/bench_pointers.cpp" % curr_path, "-o", "%s/bench_pointers" % curr_path],
                  ["%s/bench_pointers" % curr_path]),
        BenchMeta("Python", None, ["python", "%s/bench.py" % curr_path]),
        BenchMeta("PHP", None, ["php", "%s/bench.php" % curr_path]),
        BenchMeta("Node.js", None, ["node", "%s/bench.js" % curr_path])
    ]

    print("Compiling benches")
    ready_benches = []
    for bench in benches:
        exit_status = 0
        if bench.compile_command is not None:
            print("Compiling %s" % bench.name)
            exit_status = subprocess.call(bench.compile_command)
        if exit_status == 0:
            ready_benches.append(bench)
        else:
            print_error("%s failed to compile" % bench.name)

    results = {}
    for bench in ready_benches:
        process = subprocess.Popen(bench.run_command, stdout=subprocess.PIPE)
        result = ""
        while process.poll() is None:
            read_bytes = process.stdout.read1(1000000)
            if len(read_bytes) > 0:
                decode = read_bytes.decode("utf-8")
                sys.stdout.write(decode)
                result += decode
            time.sleep(0.1)
        decode = process.stdout.read().decode("utf-8")
        sys.stdout.write(decode)
        result += decode
        if process.returncode == 0:
            lines = result.splitlines()
            results[bench.name] = {
                "allocTime": extract_time(lines[-3]),
                "randomPopulTime": extract_time(lines[-2]),
                "sortTime": extract_time(lines[-1])
            }

    plot(BenchMeta.array_size, BenchMeta.number_of_benches, results)


if __name__ == "__main__":
    main()
